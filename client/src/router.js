import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Authors from './views/Authors.vue'
import Rules from './views/Rules.vue'
import Page404 from './views/Page404.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/autorzy',
      name: 'authors',
      component: Authors
    },
    {
      path: '/instrukcja',
      name: 'rules',
      component: Rules
    },
    {
      path: '*',
      name: '404',
      component: Page404

    }
  ],
  linkExactActiveClass: 'active'
})
