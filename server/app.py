import json, random
from flask import Flask, render_template, request, jsonify
from PIL import Image
import requests, numpy, io, base64, matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
app = Flask(__name__, template_folder='static', static_folder='static', static_url_path='')

#DEV ONLY
from flask_cors import CORS
CORS(app)
app.debug = True

with open('places.json', encoding="utf8") as f:
    PLACES = json.load(f)

def findById(lst, value):
    for dic in lst:
        if dic["id"] == value:
            return dic
    return -1

@app.route('/api/maps', methods=['POST'])
def check():
    if request.method == 'POST':
        post_data = request.get_json()

        answeredCity = post_data["answer"]
        placeID = post_data["placeID"]
        placeToCheck = findById(PLACES, placeID)

        if answeredCity.lower().strip() in placeToCheck["answers"]:
            result = { 'isCorrect': True, 'name': placeToCheck["name"], 'city': placeToCheck["answers"][0] }
        else: 
            result = { 'isCorrect': False }

        return jsonify({'status': 'success', 'result': result})

@app.route('/api/places', methods=['POST'])
def places():
    if request.method == 'POST':
        usedCities = request.get_json()['usedCities']
        unusedCities = [ place for place in PLACES if place['id'] not in usedCities ]
        random.shuffle(unusedCities)
        payload = random.choice(unusedCities)
        return jsonify(payload)

@app.route('/api/places/<placeID>')
def place(placeID):
    payload = findById(PLACES, int(placeID))
    return jsonify(payload)

@app.route('/api/postcard', methods=['POST'])
def postcard():
    if request.method == 'POST':
        urls = request.get_json()
        payload = {'status': 'success', 'result': makePostcard(urls)}
        return jsonify(payload)

@app.route("/", defaults={"path": ""})
@app.route("/<string:path>") 
@app.route("/<path:path>")
def index(path):
  return render_template('index.html')

def imgUrlToArray(url):
    response = requests.get(url, stream=True).raw
    img = Image.open(response)
    return numpy.array(img)

def getImgUrl(imgArr):
    imgBytes = io.BytesIO()
    plt.imshow(imgArr)
    plt.axis("off")
    plt.savefig(imgBytes, format='png', bbox_inches='tight', pad_inches=0, dpi=150)
    imgBytes.seek(0)
    url = base64.b64encode(imgBytes.getvalue()).decode()
    plt.close()
    return 'data:image/png;base64,{}'.format(url)

def makePostcard(urls):
    images = [ imgUrlToArray(url) for url in urls ]
    background = Image.open('static/pytajnik.jpg')
    for i in range(12-len(urls)):
        images.append(background)
    row = []
    for j in range(3):
        row.append(numpy.concatenate((images[0+3*j : 3+3*j]), axis=1))
    
    out = numpy.concatenate((row[0], row[1], row[2]), axis=0)
    return getImgUrl(out)




