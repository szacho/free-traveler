# Free Traveler
[https://free-traveler.herokuapp.com/](https://free-traveler.herokuapp.com/)
## Pierwsze uruchomienie 
Wymagane jest posiadanie Pythona 3 oraz [node.js](https://nodejs.org/en/).
- /client :
`npm install`, `npm run serve`
- /server :
`pip install -r requirements.txt`, `flask run`
## Wykorzystane (dodatkowe) technologie
- [Vue.js](https://vuejs.org/)
- Google Maps API
- localStorage
- [materialize.css](https://materializecss.com/)